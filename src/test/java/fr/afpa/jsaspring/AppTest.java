package fr.afpa.jsaspring;

import fr.afpa.test.controles.TestControleChoixUtilisateur;
import fr.afpa.test.controles.TestControleGeneral;
import fr.afpa.test.dao.TestDAOLecture;
import fr.afpa.test.dto.TestDTOGeneral;
import fr.afpa.test.dto.TestDTOUtilisateur;
import fr.afpa.test.services.TestServiceCreation;
import fr.afpa.test.services.TestServiceVisualisation;
import junit.extensions.ActiveTestSuite;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class AppTest extends TestCase {

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		TestSuite suite = new ActiveTestSuite();
		suite.addTest(new TestSuite(TestDAOLecture.class));
		suite.addTest(new TestSuite(TestDTOGeneral.class));
		suite.addTest(new TestSuite(TestDTOUtilisateur.class));
		suite.addTest(new TestSuite(TestServiceVisualisation.class));
		suite.addTest(new TestSuite(TestServiceCreation.class));
		suite.addTest(new TestSuite(TestControleGeneral.class));
		suite.addTest(new TestSuite(TestControleChoixUtilisateur.class));
		return suite;
	}

	public static void main(String[] args) {
		TestRunner.run(suite());
	}

}
