package fr.afpa.test.dto;

import java.util.Map;

import org.junit.Test;

import fr.afpa.dto.DTOUtilisateur;
import fr.afpa.entites.Personne;
import junit.framework.TestCase;

public class TestDTOUtilisateur extends TestCase {

	@Test
	public void testListeTousUtilisateursNonVide() {
		DTOUtilisateur dtou = new DTOUtilisateur();
		Map<Integer, Personne> listeUtilisateurs = dtou.listePersonnes();
		assertFalse("Initialisation, Table profil vide", listeUtilisateurs.isEmpty());
	}

}
