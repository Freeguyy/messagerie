package fr.afpa.test.dao;

import java.util.List;

import org.junit.Test;

import fr.afpa.dao.DAOLecture;
import fr.afpa.entitespersistees.ProfilBDD;
import junit.framework.TestCase;

public class TestDAOLecture extends TestCase {

	@Test
	public void testRecuperationListeProfils() {
		DAOLecture daol = new DAOLecture();
		List<ProfilBDD> listeProfils = daol.listeTousProfils();
		assertFalse("Initialisation, Table profil vide", listeProfils.isEmpty());
	}

}
